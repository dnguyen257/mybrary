//framework
const express = require('express')
const app = express();

const indexRouter = require('./routes/index')
const authorRouter = require('./routes/authors')
const bookRouter = require('./routes/books')
//Set environment variables
const port = process.env.PORT

const expressLayouts = require('express-ejs-layouts')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')

//Config framework
app.set('view engine','ejs')
app.set('views',__dirname + '/views')
app.set('layout','layouts/layout')
app.use(expressLayouts)
app.use(methodOverride('_method'))
app.use(express.static('public'))
app.use(express.urlencoded({ limit: '10mb', extended:false}))


//Using routers
app.use(indexRouter)
app.use('/authors',authorRouter)
app.use('/books',bookRouter)
//Confirm that server is running
app.listen(port,()=> {
    console.log('Server is running on port '+ port);
})

module.exports = app